﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TokenController : MonoBehaviour 
{
	public delegate void OnSelected(TokenController token);
	public event OnSelected onSelected;

	private bool isSelected = false;
	public bool Selected
	{
		get
		{
			return isSelected;
		}
		set
		{
			isSelected = value;
			mat.color = isSelected ? selectedColor : defaultColor;
		}
	}

	private Material mat;
	private Color defaultColor;
	private Color selectedColor = Color.cyan;

	// Use this for initialization
	void Start () 
	{
		mat = this.GetComponent<Renderer>().material;
		defaultColor = mat.color;
	}

	void OnMouseDown ()
	{
		onSelected(this);
	}
}
