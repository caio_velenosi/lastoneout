﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour 
{
	public Button buttonCollect;
	public GameObject modeSelector;
	public GameObject endScreen;
	public Text endGameMessage;

	public enum GameMode
	{
		SinglePlayer,
		MultiPlayer
	};

	private int numTokensAvailable = 20;
	public int TokensLeft {
		get 
		{
			return numTokensAvailable;	
		}
	}

	private GameMode gameMode;

	private List<TokenController> selectedTokens = new List<TokenController>();
	private List<TokenController> activeTokens;
	private TokenController[] tokens;

	private bool aiRound = true;
	private AIController aiPlayer = new AIController();

	private int currentPlayer = 0;

	// Use this for initialization
	void Start () 
	{
		aiPlayer.controller = this;
		buttonCollect.enabled = false;	
		tokens = FindObjectsOfType(typeof(TokenController)) as TokenController[];

		foreach (var token in tokens)
		{
			token.onSelected += SelectToken;
		}

		buttonCollect.gameObject.SetActive(false);
		endScreen.SetActive(false);
		modeSelector.SetActive(true);
	}

	void Update()
	{
		if (aiRound)
			aiPlayer.Update();
	}

	public void StartGame(int mode)
	{
		gameMode = (GameMode)mode;
		activeTokens = new List<TokenController>(tokens);

		if (gameMode == GameMode.SinglePlayer)
		{
			aiPlayer.Reset();

			aiRound = Random.Range(0f, 1f) > 0.5f;

			if (aiRound)
			{
				aiPlayer.PlayRound();
			}
		}
		else
		{
			aiRound = false;
			currentPlayer = 0;
		}

		buttonCollect.gameObject.SetActive(true);
		modeSelector.SetActive(false);
	}

	private void SelectToken(TokenController token)
	{
		if (aiRound)
			return;
		
		if (token.Selected)
		{
			token.Selected = false;
			selectedTokens.Remove(token);
			activeTokens.Add(token);
		}
		else
		{
			if (selectedTokens.Count == 3)
				return;
			
			token.Selected = true;
			selectedTokens.Add(token);
			activeTokens.Remove(token);
		}

		buttonCollect.enabled = selectedTokens.Count > 0;
	}

	public void SelectRandomTokens(int amount)
	{
		if (amount > activeTokens.Count)
			amount = activeTokens.Count;
		
		for (int i = 0; i < amount; i++)
		{
			TokenController token = activeTokens[Random.Range(0, activeTokens.Count)];
			token.Selected = true;
			activeTokens.Remove(token);
			selectedTokens.Add(token);
		}
	}

	public void CollectTokens()
	{
		buttonCollect.enabled = false;

		numTokensAvailable -= selectedTokens.Count;

		foreach (var token in selectedTokens)
		{
			token.gameObject.SetActive(false);
		}
		selectedTokens.Clear();

		if (numTokensAvailable == 0)
			FinishGame();
		else
			NextTurn();
	}

	private void NextTurn()
	{
		if (gameMode == GameMode.SinglePlayer)
		{
			aiRound = !aiRound;
			if (aiRound)
			{
				aiPlayer.PlayRound();
			}
		}
		else
		{
			currentPlayer = (currentPlayer + 1) % 2;
		}
	}

	private void FinishGame()
	{
		buttonCollect.gameObject.SetActive(false);
		endScreen.SetActive(true);

		if (gameMode == GameMode.SinglePlayer)
		{
			endGameMessage.text = aiRound ? "You win!" : "You lose!";
		}
		else
		{
			endGameMessage.text = "Player " + (currentPlayer + 1) + " wins!";
		}

	}

	public void Restart()
	{
		foreach (var token in tokens)
		{
			token.gameObject.SetActive(true);
			token.Selected = false;
		}

		numTokensAvailable = 20;

		endScreen.SetActive(false);
		modeSelector.SetActive(true);

		aiRound = true;
	}
}
