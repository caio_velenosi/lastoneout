﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour 
{
	public GameController controller;

	private int[] checkpoints = { 17, 13, 9, 5, 1 };
	private int currentCheckpoint = 0;

	private int tokensToCollect;

	private float timeToAction;
	private float counter;

	private enum PlayPhase
	{
		Idle,
		Select,
		Collect
	}
	private PlayPhase phase;

	public void PlayRound()
	{
		int tokensLeft = controller.TokensLeft;

		if (tokensLeft <= checkpoints[currentCheckpoint])
			currentCheckpoint++;

		if (currentCheckpoint < checkpoints.Length)
			tokensToCollect = tokensLeft - checkpoints[currentCheckpoint] <= 3 ? tokensLeft - checkpoints[currentCheckpoint] : Random.Range(1, 3);
		else
			tokensToCollect = 1;

		phase = PlayPhase.Select;
		timeToAction = Random.Range(0.4f, 1f);
		counter = 0f;
	}

	public void Reset()
	{
		currentCheckpoint = 0;
	}

	public void Update()
	{
		counter += Time.deltaTime;

		if (phase == PlayPhase.Select && counter >= timeToAction)
		{
			phase = PlayPhase.Collect;
			controller.SelectRandomTokens(tokensToCollect);

			counter = 0f;
		}
		else if (phase == PlayPhase.Collect && counter >= 0.5f)
		{
			phase = PlayPhase.Idle;
			controller.CollectTokens();
		}
	}
}
