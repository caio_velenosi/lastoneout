# Last one out
Case study game. 

## Rules
Last one out loses is a simple game where a playing surface is covered with at least
20 items (sticks, coins, etc.). There are two players, each of which may remove 1 to 3
items. The person who removes the final item to clear the playing surface loses the
game.

## AI behaviour
Starting from the only sure win case in the game (leaving a single token on the board), a simple proggression `4a + 1` gives all the movement results (checkpoints) that can be used to enforce the win situation. So the AI always tries to remove the amount of tokens needed to get to one of those results. In the impossibility of doing so, it removes a random amount and waits for the player to make a mistake that allows it to go back to the winnable proggression. 
As of now, the AI desired steps are hard coded for this board size, but it would be trivial to have it calculated at the begining of the round. 

## Fairness of the game
Given the reasoning explained above, the game clearly has an advantage for the first player, as removing 3 tokens in the first turn would make it possible to ensure all the other advantage moves, by following the pre calculated checkpoints or, in a way easier for humans, by removing 4 minus the amount of tokens removed by the other player at each round. The only way for the second player to win is counting on the first player to miss one of the checkpoints, reversing the game status. 

## Where to go from here
- Visuals could be greatly updated, both with models, animations, textures and a better UI layout. As it is, it's playable, but not pleasant. 
- Dificulty could be tweaked by assigning a probability for the AI to play according to the optimum route. In the way it is right now, it will win every game it starts. 